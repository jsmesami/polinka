# The Game of Polínka

Visit [https://polinka.web.app](https://polinka.web.app) (preferably on a cellphone)

## Development

### Prerequisites

* Java 8+
* Yarn
* [Babashka](https://github.com/babashka/babashka)
* [Clj-kondo](https://github.com/clj-kondo/clj-kondo)

### Environment

To install development environment do:

    bb dev

To run interactive development environment for ClojureScript and Sass do:

    bb watch

Open your browser at [localhost:3333](http://localhost:3333/).

### QA

To lint your ClojureScript code do:

    bb lint

To run unit tests do:

    bb test

## Build

To create a production build do:

    bb dist

## License

Copyright © 2019 Ondřej Nejedlý

Distributed under the [Apache 2.0 License with Commons Clause License Condition 1.0](LICENSE)
