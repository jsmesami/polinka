module.exports = function (config) {
    config.set({
        browsers: ['ChromeHeadlessNoSandbox'],
        basePath: 'out/test/js/',
        files: ['test.js'],
        frameworks: ['cljs-test'],
        plugins: ['karma-cljs-test', 'karma-chrome-launcher', 'karma-junit-reporter'],
        reporters: ['progress', 'junit'],
        colors: true,
        logLevel: config.LOG_INFO,
        client: {
            args: ['shadow.test.karma.init'],
            singleRun: true
        },
        customLaunchers: {
            ChromeHeadlessNoSandbox: {
                base: 'ChromeHeadless',
                flags: ['--no-sandbox']
            }
        },
    })
};
