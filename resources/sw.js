const CACHE_NAME = 'polinka-cache';

const cachedUrls = //{ cached-urls }

async function cacheResources(cacheName, resources) {
    const cache = await caches.open(cacheName);
    return cache.addAll(resources);
}

async function clearOldCache(cacheName) {
    const cacheNames = await caches.keys();
    const oldCacheName = cacheNames.find(name => name !== cacheName);
    await caches.delete(oldCacheName);
}

async function getResponseByRequest(cacheName, request) {
    const cache = await caches.open(cacheName);
    const cachedResponse = await cache.match(request);
    return cachedResponse || fetch(request);
}

self.addEventListener("install", e => {
    self.skipWaiting();
    e.waitUntil(cacheResources(CACHE_NAME, cachedUrls));
});

self.addEventListener("activate", e => {
    e.waitUntil(clearOldCache(CACHE_NAME));
});

self.addEventListener("fetch", e => {
    e.respondWith(getResponseByRequest(CACHE_NAME, e.request));
});
