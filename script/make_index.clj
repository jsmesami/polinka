(ns make-index
  "Generate index.html for respective environment."
  (:require
    [clojure.string]
    [shared]))


(let [[env] *command-line-args*]
  (shared/require-env env)
  (let [dst-root (shared/root-for-env env)
        dist?     (= env "dist")
        messages  (read-string (slurp "resources/messages.edn"))
        statics   (read-string (slurp "resources/static.edn"))
        statics   (if dist?
                    statics
                    (zipmap (keys statics) (repeat nil)))
        root      (partial str "/")
        generated (if dist?
                    (->> shared/generated
                         (map (partial shared/generated->hashed dst-root)))
                    shared/generated)]
    (->> (slurp "resources/index.html")
         (shared/replace-vars
           (merge
             {:data-messages (pr-str messages)
              :data-statics  (pr-str statics)}
             (zipmap shared/generated-vars
                     (map root generated))))
         (shared/replace-statics statics)
         (spit (str dst-root "index.html")))))
