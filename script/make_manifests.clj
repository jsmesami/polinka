(ns make-manifests
  "Generate polinka.webmanifest and browserconfig.xml for respective environment."
  (:require
    [clojure.string]
    [shared]))


(let [[env] *command-line-args*]
  (shared/require-env env)
  (let [dst-root (shared/root-for-env env)
        dist?    (= env "dist")
        statics  (read-string (slurp "resources/static.edn"))
        statics  (if dist?
                   statics
                   (zipmap (keys statics) (repeat nil)))]
    (doseq [manifest ["polinka.webmanifest" "browserconfig.xml"]]
      (->> (str "resources/" manifest)
           slurp
           (shared/replace-statics statics)
           (spit (str dst-root manifest))))))
