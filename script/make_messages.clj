(ns make-messages
  "Traverse source files, search for translatable messages, update messages.edn."
  (:require
    [clojure.java.io :as io]
    [clojure.pprint]
    [clojure.set]))


(defn search-trns
  [file]
  (->> (slurp file)
       (re-seq #"\[tr\s*\"([^\"]+)\"\s*\]")
       (map second)))


(defn find-keys
  [dir]
  (->> (io/file dir)
       file-seq
       (filter #(.isFile %))
       (map search-trns)
       (filter seq)
       (apply concat)
       (apply sorted-set)
       (apply vector)))


(defn get-old-trns
  [trns-path]
  (try
    (read-string (slurp trns-path))
    (catch Exception _
      {})))


(defn save-new-trns
  [trns-path data]
  (->> data
       clojure.pprint/pprint
       with-out-str
       (spit trns-path)))


(let [[lang] *command-line-args*]
  (when (empty? lang)
    (println "Missing required argument: <lang>")
    (System/exit 1))
  (let [trns-path   "resources/messages.edn"
        old-trns    (get-old-trns trns-path)
        found-keys  (find-keys "src")
        excess-keys (clojure.set/difference (set (keys old-trns)) (set found-keys))]
    (->> found-keys
         (reduce
           (fn [acc key]
             (update-in acc [key lang] #(or (not-empty %) "")))
           (apply dissoc old-trns excess-keys))
         (into (sorted-map))
         (save-new-trns trns-path))))
