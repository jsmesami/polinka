(ns make-static
  "Traverse static files, copy them to respective environment, generate report file."
  (:require
    [clojure.java.io :as io]
    [clojure.java.shell :as shell]
    [clojure.pprint]
    [clojure.string]
    [shared]))


(let [[env] *command-line-args*]
  (shared/require-env env)
  (let [dst-root  (shared/root-for-env env)
        src-root  "resources/static/"
        src-files (->> (io/file src-root)
                       file-seq
                       (filter #(.isFile %)))
        uris      (->> src-files
                       (map #(.getPath %))
                       (map #(clojure.string/replace % src-root "/")))
        hashes    (map (comp hash slurp) src-files)]
    (shell/sh "rsync" "-r" src-root dst-root)
    (->> (map vector uris hashes)
         (into (sorted-map))
         clojure.pprint/pprint
         with-out-str
         (spit "resources/static.edn"))))
