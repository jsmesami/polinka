(ns make-sw
  "Generate sw.js for respective environment."
  (:require
    [cheshire.core :as json]
    [shared]))


(let [[env] *command-line-args*]
  (shared/require-env env)
  (let [dst-root  (shared/root-for-env env)
        dist?     (= env "dist")
        statics   (read-string (slurp "resources/static.edn"))
        statics   (if dist?
                    (->> (seq statics)
                         (map (partial apply shared/with-ver)))
                    (keys statics))
        root      (partial str "/")
        generated (if dist?
                    (->> shared/generated
                         (map (partial shared/generated->hashed dst-root)))
                    shared/generated)
        cached    (sort (concat ["/"] statics (map root generated)))]
    (->> (slurp "resources/sw.js")
         (shared/replace-vars
           {:cached-urls (json/generate-string (sort cached) {:pretty true})})
         (spit (str dst-root "sw.js")))))
