(ns shared
  "Utilities shared between scripts"
  (:require
    [clojure.string]))


(defn require-env
  [env]
  (when (empty? env)
    (println "Missing required argument: <env>")
    (System/exit 1))
  (when-not (#{"dev" "dist"} env)
    (println "Invalid env:" env)
    (System/exit 1)))


(defn with-ver
  [uri ver]
  (if ver
    (format "%s?v=%s" uri ver)
    uri))


(def root-for-env
  (partial format "out/%s/"))


(def generated
  ["js/polinka.js"
   "css/polinka.css"
   "polinka.webmanifest"
   "browserconfig.xml"])


(def generated-vars
  [:app-js
   :styles
   :webmanifest
   :browserconfig])


(defn generated->hashed
  [dst-root generated]
  (->> (str dst-root generated)
       slurp
       hash
       (with-ver generated)))


(defn replace-vars
  "Replace variables in a template using variable->value mapping"
  [vars-map template]
  (reduce-kv
    (fn [acc k v]
      (let [match (re-pattern (format "//\\{\\s*%s\\s*\\}" (name k)))]
        ;; Replace pattern: //{ variable }
        (clojure.string/replace acc match v)))
    template
    vars-map))


(defn replace-statics
  "Replace static files in a template using static->version mapping "
  [statics-map template]
  (reduce-kv
    (fn [acc k v]
      (let [match (re-pattern (format "//\\{\\{\\s*%s\\s*\\}\\}" k))]
        ;; Replace pattern: //{{ /static/file }}
        (clojure.string/replace acc match (with-ver k v))))
    template
    statics-map))
