(ns polinka.analytics.db
  (:require
    ["firebase/app" :as f-app]
    ["firebase/analytics" :as f-analytics]))


(def config #js {:apiKey "AIzaSyB16AvcuPMwS5u8uocmrwnTj0x48GEobBA"
                 :authDomain "polinka.web.app"
                 :databaseURL "https://polinka.firebaseio.com"
                 :projectId "polinka"
                 :storageBucket "polinka.appspot.com"
                 :messagingSenderId "506724011863"
                 :appId "1:506724011863:web:482d2b2165d8b265fcf812"})

(defonce analytics
         (-> config
             f-app/initializeApp
             f-analytics/getAnalytics))
