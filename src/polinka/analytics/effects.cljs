(ns polinka.analytics.effects
  (:require
    [re-frame.core :refer [reg-fx]]
    [polinka.analytics.db :as db]
    ["firebase/analytics" :as f-analytics]))


(reg-fx
  ::event
  (fn [[name params]]
    (try
      (if params
        (f-analytics/logEvent db/analytics name (clj->js params))
        (f-analytics/logEvent db/analytics name))
      (catch js/Error. e
        (js/console.error e)))))
