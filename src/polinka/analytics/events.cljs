(ns polinka.analytics.events
  (:require
    [polinka.analytics.effects :as-alias analytics]
    [re-frame.core :refer [reg-event-fx trim-v]]))


(reg-event-fx
  ::event
  [trim-v]
  (fn [_ [name params]]
    {:pre [name]}
    {::analytics/event [name params]}))
