(ns polinka.app.core
  "App base"
  (:require
    [polinka.app.effects]
    [polinka.app.events]))
