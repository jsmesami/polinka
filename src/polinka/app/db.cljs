(ns polinka.app.db)


(defonce
  initial-db
  {:init/wait-for #{:preload :translate :sounds}})
