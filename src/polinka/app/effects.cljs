(ns polinka.app.effects
  (:require
    [re-frame.core :refer [reg-cofx]]))


(reg-cofx
  ::timestamp
  (fn [cofx]
    (assoc cofx :timestamp (.now js/Date))))
