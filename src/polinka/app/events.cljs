(ns polinka.app.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [polinka.app.db :refer [initial-db]]
    [polinka.app.effects :as-alias app-fx]
    [polinka.config :refer [intro-timeout]]
    [polinka.preload.events :as-alias preload]
    [polinka.screens.events :as-alias screens]
    [polinka.sound.events :as-alias sound]
    [polinka.sw.effects :as-alias sw]
    [polinka.translate.events :as-alias translate]
    [re-frame.core :refer [inject-cofx reg-event-fx trim-v]]))


(reg-event-fx
  ::init
  [(inject-cofx ::app-fx/timestamp)]
  (fn-traced [{timestamp :timestamp}]
    {:db (assoc initial-db :init/start-t timestamp)
     :fx [[::sw/init "/sw.js"]
          [:dispatch [::preload/init]]
          [:dispatch [::translate/init]]
          [:dispatch [::sound/init]]
          [:dispatch [::screens/set :intro]]]}))


(reg-event-fx
  ::ready!
  [(inject-cofx ::app-fx/timestamp) trim-v]
  (fn-traced [{:keys [db timestamp]} [module]]
    {:db (-> db
             (update :init/wait-for disj module)
             (update :init/stop-t max timestamp))
     :dispatch [::maybe-launch]}))


(reg-event-fx
  ::maybe-launch
  (fn-traced [{{:keys [:init/wait-for :init/start-t :init/stop-t]} :db}]
    (let [elapsed (- stop-t start-t)
          timeout (- intro-timeout elapsed)]
      (when (empty? wait-for)
        (if (pos? timeout)
          {:dispatch-later {:ms timeout, :dispatch [::screens/set :ballot]}}
          {:dispatch [::screens/set :ballot]})))))


(reg-event-fx
  ::noop
  (fn-traced []
    {}))
