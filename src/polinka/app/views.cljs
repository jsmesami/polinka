(ns polinka.app.views
  (:require
    [polinka.screens.base :refer [base-view]]))


(defn app
  []
  [base-view])
