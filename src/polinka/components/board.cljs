(ns polinka.components.board
  (:require
    ["react-minimal-pie-chart" :refer [PieChart]]
    [goog.events :as gevents]
    [goog.events.EventType :refer [RESIZE]]
    [polinka.components.link :refer [link]]
    [polinka.config :refer [info-field-id]]
    [polinka.game.events :as-alias game-events]
    [polinka.game.subs :as-alias game-subs]
    [polinka.utils.bem :refer [be bem]]
    [polinka.utils.debounce :refer [debounce]]
    [polinka.utils.geometry :refer [fit-rectangle-to-container]]
    [re-frame.core :refer [dispatch subscribe]]
    [reagent.core :as r]
    [reagent.format :refer [format]]))


(def module-name :board)


(defn- fields->rows
  [logs]
  (let [indexes (cycle [4 3])]
    (loop [[row remaining] (split-at (first indexes) logs)
           indexes (next indexes)
           result []]
      (if (empty? row)
        result
        (recur (split-at (first indexes) remaining)
               (next indexes)
               (conj result row))))))


(defn- pie
  []
  [:svg
   {:viewBox "0 0 187 187"}
   [:use
    {:xlinkHref "#pie"}]])


(defn- progress-chart
  [progress]
  (let [props {:class-name (be module-name :chart)
               :startAngle 270
               :totalValue 100
               :data [{:value progress
                       :color "black"}]}]
    [:> PieChart props]))


(defmulti hexagon :id)


(defmethod hexagon info-field-id
  [{:keys [id event-id]}]
  (let [progress @(subscribe [::game-subs/game-progress])]
    [link
     [:div
      {:class (be module-name :image)}
      [pie]
      [progress-chart progress]]
     {:on-click #(dispatch [event-id id])
      :class (bem module-name :hexagon :info)}]))


(defmethod hexagon :default
  [{:keys [id image-id taken? selected? event-id rotation]}]
  [link
   [:svg
    {:class (be module-name :image)
     :viewBox "0 0 187 187"
     :style {:transform (format "rotate(%ddeg)" rotation)}}
    [:use
     {:xlinkHref (str "#" (name image-id))}]]
   {:on-click #(dispatch [event-id id])
    :class (bem module-name :hexagon [(when taken? :taken)
                                      (when selected? :selected)])}])


(defn- row
  [rw]
  [:div
   {:class (be module-name :row)}
   (for [field rw]
     ^{:key (:id field)}
     [hexagon field])])


(defn board
  [_fields]
  (let [listener-key (atom nil)
        dom-node (atom nil)
        board-scale (subscribe [::game-subs/board-scale])
        resize-board
        (fn [cont]
          (let [rect (-> cont .-children first)
                rect_size [(.-offsetWidth rect) (.-offsetHeight rect)]
                cont_size [(.-offsetWidth cont) (.-offsetHeight cont)]
                [old_w _] rect_size
                [new_w _] (fit-rectangle-to-container rect_size cont_size)]
            (dispatch [::game-events/set-board-scale (/ new_w old_w)])))]
    (r/create-class
      {:component-did-mount
       (fn [_]
         (let [do-resize (partial resize-board @dom-node)
               listener (debounce do-resize 250)]
           (do-resize)
           (reset! listener-key (gevents/listen js/window RESIZE listener))))
       :component-will-unmount
       #(gevents/unlistenByKey @listener-key)
       :reagent-render
       (fn [fields]
         [:div
          {:ref #(reset! dom-node %)
           :class (be module-name :wrapper)}
          [:div
           {:class module-name
            :style {:transform (format "scale(%.2f)" @board-scale)
                    :transition "transform 100ms"}}
           (for [[idx rw] (map-indexed vector (fields->rows fields))]
             ^{:key idx}
             [row rw])]])})))
