(ns polinka.components.core
  "UI Components"
  (:require
    [polinka.components.board]
    [polinka.components.link]
    [polinka.components.new-version]
    [polinka.components.score]
    [polinka.components.select-game]
    [polinka.components.settings]
    [polinka.components.timer]))


(def board polinka.components.board/board)

(def link polinka.components.link/link)

(def new-version polinka.components.new-version/new-version)

(def score polinka.components.score/score)

(def select-game polinka.components.select-game/select-game)

(def settings polinka.components.settings/settings)

(def timer polinka.components.timer/timer)
(def meter polinka.components.timer/meter)
