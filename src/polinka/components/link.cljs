(ns polinka.components.link)


(defn link
  [child & {:keys [on-click class]}]
  [:a.link
   {:href "#"
    :class class
    :on-click #(do (.preventDefault %)
                   (when on-click
                     (on-click)))}
   child])
