(ns polinka.components.new-version
  (:require
    [polinka.components.link :refer [link]]
    [polinka.sw.subs :as-alias sw]
    [polinka.translate.views :refer [tr]]
    [re-frame.core :refer [subscribe]]))


(defn new-version
  []
  (when @(subscribe [::sw/new-version?])
    [:div.new-version
     [:p
      [:span.flashy
       [tr "New version available"]]
      [:br]
      [link
       [tr "Install"]
       {:on-click #(js/location.reload false)}]]]))
