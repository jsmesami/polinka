(ns polinka.components.score
  (:require
    [polinka.translate.views :refer [tr]]
    [reagent.format :refer [format]]))


(def module-name :score)


(defn score
  [score']
  [:div
   {:class module-name}
   [tr "Score"] " "
   [:span.text-secondary (format "%04d" score')]])
