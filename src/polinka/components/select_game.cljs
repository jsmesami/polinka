(ns polinka.components.select-game
  (:require
    [polinka.analytics.events :as-alias analytics]
    [polinka.components.link :refer [link]]
    [polinka.screens.events :as-alias screens]
    [polinka.translate.views :refer [tr]]
    [re-frame.core :refer [dispatch]]))


(defn select-game
  [title]
  [:div.select-game
   [:p title]
   [link
    [tr "Game 1"]
    {:class :link--button
     :on-click #(do (dispatch [::screens/set :game1-pairs])
                    (dispatch [::analytics/event "select_content"
                               {:content_type "Pairs"}]))}]
   [:p.text-secondary.text-smaller
    [tr "Find matching log pairs"]]
   [link
    [tr "Game 2"]
    {:class :link--button
     :on-click #(do (dispatch [::screens/set :game2-six])
                    (dispatch [::analytics/event "select_content"
                               {:content_type "Six"}]))}]
   [:p.text-secondary.text-smaller
    [tr "Find six identical logs"]]])
