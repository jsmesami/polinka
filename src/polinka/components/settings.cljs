(ns polinka.components.settings
  (:require
    [polinka.components.link :refer [link]]
    [polinka.screens.events :as-alias screens]
    [polinka.translate.views :refer [tr]]
    [re-frame.core :refer [dispatch]]))


(defn settings
  []
  [:div.bottom-link
   [link
    [tr "Settings"]
    {:on-click #(dispatch [::screens/set :settings])}]])
