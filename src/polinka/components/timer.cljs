(ns polinka.components.timer
  (:require
    [polinka.config :refer [ms-to-fail]]
    [polinka.utils.bem :refer [be]]
    [reagent.format :refer [format]]))


(def module-name :timer)


(defn meter
  [pct]
  [:div
   {:class module-name}
   [:div
    {:class (be module-name :bar)
     :style {:width (format "%d%" pct)}}]])


(defn timer
  [countdown]
  (let [pct (-> (/ countdown ms-to-fail)
                (.toFixed 1)
                (* 100))]
    [meter pct]))
