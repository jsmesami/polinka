(ns polinka.config)

(def ^:const intro-timeout
  (if js/goog.DEBUG 0 4000))

(def ^:const ms-to-fail 10000)

(def ^:const countdown-freq 50)

(def ^:const info-field-id 99)

(def ^:const sound-on? true)

(def ^:const language "en")

(def ^:const supported-languages (sorted-set "en" "cs"))
