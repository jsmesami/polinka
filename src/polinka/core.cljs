(ns polinka.core
  (:require
    [polinka.analytics.core]
    [polinka.app.core]
    [polinka.app.events :as-alias app]
    [polinka.app.views :as views]
    [polinka.game.core]
    [polinka.preload.core]
    [polinka.screens.core]
    [polinka.sound.core]
    [polinka.sw.core]
    [polinka.storage.core]
    [polinka.translate.core]
    [reagent.dom]
    [re-frame.core :as rf]
    [re-frame-flow.core :as re-flow]))


(defn- render!
  []
  (reagent.dom/render
    [#'views/app]
    (js/document.getElementById "app")))


(defn reload!
  []
  (re-flow/clear-cache!)
  (rf/clear-subscription-cache!)
  (render!))


(defn ^:export main
  []
  (rf/dispatch-sync [::app/init])
  (render!))
