(ns polinka.game.data
  (:require
    [reagent.format :refer [format]]))


(defonce image-ids
         (map #(keyword (format "polinko-%02d" %)) (range 1 49)))


(defrecord Field [id image-id event-id rotation selected? taken?])
