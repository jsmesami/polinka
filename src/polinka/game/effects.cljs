(ns polinka.game.effects
  (:require
    [polinka.analytics.events :as-alias analytics]
    [polinka.config :refer [countdown-freq]]
    [polinka.game.events :as-alias game]
    [polinka.screens.events :as-alias screens]
    [re-frame.core :refer [dispatch reg-fx]]))


(def countdown-timer (atom nil))


(reg-fx
  ::start-countdown
  (fn [game]
    (reset! countdown-timer
            (js/setInterval #(dispatch [::game/countdown game countdown-freq]) countdown-freq))))


(reg-fx
  ::stop-countdown
  (fn []
    (js/clearInterval @countdown-timer)
    (reset! countdown-timer nil)))


(reg-fx
  ::animate-score
  (fn [score]
    (let [frames (repeatedly 4 (partial rand-int score))]
      (doall
        (for [[i s] (map-indexed vector (conj (vec frames) score))]
          (js/setTimeout #(dispatch [::game/set-score s])
                         (* i countdown-freq)))))))


(reg-fx
  ::success
  (fn [[game score]]
    (dispatch [::screens/set :game-over {:status :won}])
    (dispatch [::analytics/event "player_won"
               {:game game
                :score score}])))


(reg-fx
  ::fail
  (fn [game]
    (dispatch [::screens/set :game-over {:status :lost}])
    (dispatch [::analytics/event "player_lost"
               {:game game}])))
