(ns polinka.game.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [polinka.game.effects :as-alias game]
    [re-frame.core :refer [reg-event-db reg-event-fx trim-v]]))


(reg-event-fx
  ::countdown
  [trim-v]
  (fn-traced [{db :db} [game decrement]]
    (let [curr-count (get-in db [:model :countdown])
          next-count (- curr-count decrement)]
      (if (neg? next-count)
        {::game/stop-countdown nil
         ::game/fail game}
        {:db (assoc-in db [:model :countdown] next-count)}))))


(reg-event-db
  ::set-score
  [trim-v]
  (fn-traced [db [score]]
    (assoc-in db [:model :score] score)))


(reg-event-db
  ::set-board-scale
  [trim-v]
  (fn-traced [db [scale]]
    (assoc-in db [:board :scale] scale)))
