(ns polinka.game.subs
  (:require
    [re-frame.core :refer [reg-sub]]))


(reg-sub
  ::game-progress
  (fn [db]
    (get-in db [:model :progress])))


(reg-sub
  ::board-scale
  (fn [db]
    (get-in db [:board :scale] 1.)))
