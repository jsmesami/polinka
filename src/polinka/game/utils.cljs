(ns polinka.game.utils
  (:require
    [polinka.config :refer [info-field-id]]))


(defn random-rotation
  [old-rotation]
  (let [new-rotation (-> (rand-int 6) inc (* 60))]
    (if-not (= new-rotation old-rotation)
      new-rotation
      (recur old-rotation))))


(defn rotate-fields
  [fields]
  (let [info-field? (comp #{info-field-id} :id)
        rotate-field #(update % :rotation random-rotation)]
    (map #(if-not (info-field? %) (rotate-field %) %) fields)))


(defn partition-fields-by-id
  [fields field-id]
  (if (-> fields first :id (= field-id))
    [nil [(first fields)] (rest fields)]
    (partition-by (comp #{field-id} :id) fields)))


(defn update-model
  [db model]
  (update db :model merge model))
