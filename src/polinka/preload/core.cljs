(ns polinka.preload.core
  "Prefetching images before the game can start"
  (:require
    [polinka.preload.effects]
    [polinka.preload.events]))
