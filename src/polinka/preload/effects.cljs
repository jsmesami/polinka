(ns polinka.preload.effects
  (:require
    [polinka.app.events :as-alias app]
    [polinka.analytics.events :as-alias analytics]
    [re-frame.core :refer [dispatch reg-fx]]))


(reg-fx
  ::fetch-images
  (fn [[container images]]
    (-> (js/Promise.all
          (map (fn [url]
                 (-> (js/fetch url)
                     (.then #(.text %))))
               images))
        (.then
          (fn [texts]
            (doseq [t texts]
              (.insertAdjacentHTML container "beforeend" t))
            (dispatch [::app/ready! :preload])))
        (.catch
          (fn [err]
            (dispatch [::analytics/event "failed_images_fetching" {:err err}]))))))
