(ns polinka.preload.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [polinka.preload.effects :as-alias preload]
    [polinka.utils.static :refer [static]]
    [re-frame.core :refer [reg-event-fx]]))


(reg-event-fx
  ::init
  (fn-traced []
    (let [container (js/document.getElementById "svg-container")
          images (map static ["/images/polinka.svg" "/images/pie.svg"])]
      {::preload/fetch-images [container images]})))
