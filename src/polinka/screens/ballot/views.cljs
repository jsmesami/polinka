(ns polinka.screens.ballot.views
  (:require
    [polinka.components.core :refer [new-version select-game settings]]
    [polinka.translate.views :refer [tr]]))


(defn ballot
  []
  [:div.ballot-screen
   [:h1 "Polínka"]
   [select-game [tr "Select game:"]]
   [new-version]
   [settings]])
