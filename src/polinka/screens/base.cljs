(ns polinka.screens.base
  (:require
    [polinka.screens.ballot.views :refer [ballot]]
    [polinka.screens.game1-pairs.views :refer [game1-pairs]]
    [polinka.screens.game2-six.views :refer [game2-six]]
    [polinka.screens.game-over.views :refer [game-over]]
    [polinka.screens.intro.views :refer [intro]]
    [polinka.screens.settings.views :refer [settings]]
    [polinka.screens.subs :as-alias screens]
    [re-frame.core :refer [subscribe]]))


(def view-id->view
  {:ballot ballot
   :game1-pairs game1-pairs
   :game2-six game2-six
   :game-over game-over
   :intro intro
   :settings settings})


(defn base-view
  []
  (when-let [model @(subscribe [::screens/model])]
    [(view-id->view (:view-id model)) model]))
