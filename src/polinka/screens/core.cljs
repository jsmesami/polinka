(ns polinka.screens.core
  "App screens and view switching"
  (:require
    [polinka.screens.ballot.events]
    [polinka.screens.events]
    [polinka.screens.game1-pairs.events]
    [polinka.screens.game2-six.events]
    [polinka.screens.game-over.events]
    [polinka.screens.intro.events]
    [polinka.screens.settings.events]
    [polinka.screens.subs]))
