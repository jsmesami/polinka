(ns polinka.screens.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [re-frame.core :refer [reg-event-fx trim-v]]
    [reagent.format :refer [format]]))


(defn- view-id->init-event
  [view-id]
  (->> (name view-id)
       (format "polinka.screens.%s.events/init")
       keyword))


(reg-event-fx
  ::set
  [trim-v]
  (fn-traced [_ [view-id model]]
    {:dispatch [(view-id->init-event view-id)
                (assoc model :view-id view-id)]}))
