(ns polinka.screens.game1-pairs.db
  (:require
    [polinka.app.events :as-alias app]
    [polinka.config :refer [info-field-id ms-to-fail]]
    [polinka.game.data :refer [Field image-ids]]
    [polinka.game.utils :refer [random-rotation]]
    [polinka.screens.game1-pairs.events :as-alias game]))


(defn- prepare-image-ids
  [iids]
  (->> iids
       shuffle
       (take 12)
       (repeat 2)
       flatten
       shuffle))


(defn- init-fields
  [iids]
  (let [constructor (fn [id iid] (Field. id iid ::game/tap-field (random-rotation nil) nil nil))
        info-field (Field. info-field-id nil ::app/noop 0 nil nil)]
    (->> iids
         (map-indexed constructor)
         (split-at 12)
         (interpose [info-field])
         (apply concat))))


(defn init-model
  []
  (let [iids (prepare-image-ids image-ids)]
    {:fields (init-fields iids)
     :taken-count 0
     :progress 0
     :countdown ms-to-fail
     :score 0}))
