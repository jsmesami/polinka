(ns polinka.screens.game1-pairs.events
  (:require
    [cljs.math :as math]
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [polinka.config :refer [ms-to-fail]]
    [polinka.game.effects :as-alias game]
    [polinka.game.utils :refer [partition-fields-by-id rotate-fields update-model]]
    [polinka.screens.game1-pairs.db :refer [init-model]]
    [polinka.sound.effects :as-alias sound]
    [re-frame.core :refer [reg-event-fx trim-v]]))


(reg-event-fx
  ::init
  [trim-v]
  (fn-traced [{db :db} [model]]
    {::game/start-countdown "Pairs"
     :db (assoc db :model (merge model (init-model)))}))


(def max-taken-count 24)


(defn- calculate-score
  [countdown taken-count]
  (let [countdown-s (/ countdown 1000)
        power (* (- 25 taken-count) 0.18)]
    (math/round (math/pow countdown-s power))))


(defn advance-game
  [db tapped-field-id]
  (let [model (:model db)
        fields (:fields model)
        [before [tapped] after] (partition-fields-by-id fields tapped-field-id)
        selected (first (filter :selected? fields))]
    (if selected
      ;; First field of a pair already selected
      (if-not (or (:taken? tapped)
                  (:selected? tapped)
                  (not= (:image-id selected)
                        (:image-id tapped)))
        ;; Tapped field matches first of a pair
        (let [tapped (assoc tapped :selected? true)
              fields (->> (concat before [tapped] after)
                          (map #(cond-> % (:selected? %) (merge {:selected? false :taken? true}))))
              taken-count (count (filter :taken? fields))
              progress (* 100 (/ taken-count max-taken-count))
              score (-> (calculate-score (:countdown model) taken-count)
                        (+ (:score model)))
              won? (= taken-count max-taken-count)
              model {:fields fields
                     :taken-count taken-count
                     :progress progress
                     :countdown ms-to-fail
                     :score score}
              effects {::game/stop-countdown nil
                       :db (-> db (update-model model))}]
          (if won?
            ;; Took last pair
            (assoc effects ::game/success ["Pairs" score])
            ;; Took a pair
            (assoc effects ::sound/play :hit2
                           ::game/start-countdown "Pairs"
                           ::game/animate-score score)))
        ;; Tapped field doesn't match first of a pair
        (let [tapped (assoc tapped :selected? false)
              fields (rotate-fields (concat before [tapped] after))]
          {::sound/play :missed
           :db (-> db (update-model {:fields fields}))}))
      ;; No field is selected yet
      (if-not (:taken? tapped)
        ;; Select first of a pair
        (let [tapped (assoc tapped :selected? true)
              fields (concat before [tapped] after)]
          {::sound/play :hit1
           :db (-> db (update-model {:fields fields}))})
        ;; The field is already taken
        (let [fields (rotate-fields (:fields model))]
          {::sound/play :missed
           :db (-> db (update-model {:fields fields}))})))))


(reg-event-fx
  ::tap-field
  [trim-v]
  (fn-traced [{db :db} [field-id]]
    (advance-game db field-id)))
