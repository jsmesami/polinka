(ns polinka.screens.game1-pairs.views
  (:require
    [polinka.components.core :as components]))


(defn game1-pairs
  [{:keys [countdown fields score]}]
  [:<>
   [components/timer countdown]
   [components/board fields]
   [components/score score]])
