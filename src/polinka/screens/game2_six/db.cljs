(ns polinka.screens.game2-six.db
  (:require
    [polinka.app.events :as-alias app]
    [polinka.config :refer [info-field-id ms-to-fail]]
    [polinka.game.data :refer [Field image-ids]]
    [polinka.game.utils :refer [random-rotation]]
    [polinka.screens.game2-six.events :as-alias game]))


(defn- prepare-image-ids
  [iids]
  (let [shuffled (shuffle iids)
        reference-iid (first shuffled)
        target (repeat 6 reference-iid)
        misc (take 18 (rest shuffled))
        iids (shuffle (into target misc))]
    [reference-iid iids]))


(defn- init-fields
  [iids]
  (let [constructor (fn [id iid] (Field. id iid ::game/tap-field (random-rotation nil) false false))
        info-field (Field. info-field-id nil ::app/noop 0 false false)]
    (->> iids
         (map-indexed constructor)
         (split-at 12)
         (interpose [info-field])
         (apply concat))))


(defn init-model
  []
  (let [[reference-iid iids] (prepare-image-ids image-ids)]
    {:reference-image-id reference-iid
     :fields (init-fields iids)
     :taken-count 0
     :progress 0
     :countdown ms-to-fail
     :score 0}))
