(ns polinka.screens.game2-six.events
  (:require
    [cljs.math :as math]
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [polinka.config :refer [ms-to-fail]]
    [polinka.game.effects :as-alias game]
    [polinka.game.utils :refer [partition-fields-by-id rotate-fields update-model]]
    [polinka.screens.game2-six.db :refer [init-model]]
    [polinka.sound.effects :as-alias sound]
    [re-frame.core :refer [reg-event-fx trim-v]]))


(reg-event-fx
  ::init
  [trim-v]
  (fn-traced [{db :db} [model]]
    {::game/start-countdown "Six"
     :db (assoc db :model (merge model (init-model)))}))


(def max-taken-count 6)


(defn- calculate-score
  [countdown taken-count]
  (let [countdown-s (/ countdown 1000)
        power (if (= taken-count 1) 4 3)]
    (math/round (math/pow countdown-s power))))


(defn advance-game
  [db tapped-field-id]
  (let [model (:model db)
        [before [tapped] after] (partition-fields-by-id (:fields model) tapped-field-id)]
    (if (and (not (:taken? tapped))
             (= (:reference-image-id model) (:image-id tapped)))
      ;; Tapped field matches one of 6
      (let [tapped (assoc tapped :taken? true)
            fields (concat before [tapped] after)
            taken-count (count (filter :taken? fields))
            progress (* 100 (/ taken-count max-taken-count))
            score (-> (calculate-score (:countdown model) taken-count)
                      (+ (:score model)))
            won? (= taken-count max-taken-count)
            model {:fields fields
                   :taken-count taken-count
                   :progress progress
                   :countdown ms-to-fail
                   :score score}
            effects {::game/stop-countdown nil
                     :db (-> db (update-model model))}]
        (if won?
          ;; Took last one of matching 6
          (assoc effects ::game/success ["Six" score])
          ;; Took one of matching 6
          (assoc effects ::sound/play :hit2
                         ::game/start-countdown "Six"
                         ::game/animate-score score)))
      ;; Tapped field doesn't match one of 6
      (let [fields (rotate-fields (:fields model))]
        {::sound/play :missed
         :db (-> db (update-model {:fields fields}))}))))


(reg-event-fx
  ::tap-field
  [trim-v]
  (fn-traced [{db :db} [field-id]]
    (advance-game db field-id)))
