(ns polinka.screens.game2-six.views
  (:require
    [polinka.components.core :as components]))


(defn game2-six
  [{:keys [countdown fields score]}]
  [:<>
   [components/timer countdown]
   [components/board fields]
   [components/score score]])
