(ns polinka.screens.game-over.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [polinka.sound.effects :as-alias sound]
    [polinka.storage.effects :as-alias storage]
    [re-frame.core :refer [inject-cofx reg-event-fx trim-v]]))


(reg-event-fx
  ::init
  [(inject-cofx ::storage/get :hi-scores) trim-v]
  (fn-traced [{:keys [db hi-scores]} [model]]
    (let [old-model (:model db)
          score     (get old-model :score 0)
          game-id   (get old-model :view-id :unknown)
          hi-score  (get hi-scores game-id 0)
          status    (:status model)
          new-model {:score score
                     :hi-score hi-score}]
      (cond-> {::sound/play status
               :db (assoc db :model (merge model new-model))}

              (and (= status :won) (> score hi-score))
              (assoc ::storage/set [:hi-scores (assoc hi-scores game-id score)])))))
