(ns polinka.screens.game-over.views
  (:require
    [polinka.components.core :refer [new-version select-game settings]]
    [polinka.translate.views :refer [tr]]
    [polinka.utils.bem :refer [bm]]))


(def module-name :game-over)


(defmulti game-over :status)


(defn- won-best
  [score hi-score]
  [:<> [:h1.flashy [tr "Your record!"]]
       [:p [tr "Your score is:"] " "
        [:span.text-secondary score]]
       [:p [tr "Your best score was:"] " "
        [:span.text-secondary hi-score]]])


(defn- won
  [score hi-score]
  [:<> [:h1 [tr "You won!"]]
       [:p [tr "Your score is:"] " "
        [:span.text-secondary score]]
       (when (> hi-score 0)
         [:p [tr "Your best score is:"] " "
          [:span.text-secondary hi-score]])])


(defmethod game-over :lost
  [{:keys [hi-score]}]
  [:div
   {:class (bm module-name :lost)}
   [:h1 [tr "Game over"]]
   (when (> hi-score 0)
     [:p [tr "Your best score is:"] " "
      [:span.text-secondary hi-score]])
   [select-game [tr "Play again:"]]
   [new-version]
   [settings]])


(defmethod game-over :won
  [{:keys [hi-score score]}]
  [:div
   {:class (bm module-name :won)}
   (if (and (> score hi-score) (> hi-score 0))
     [won-best score hi-score]
     [won score hi-score])
   [select-game [tr "Play again:"]]
   [new-version]
   [settings]])
