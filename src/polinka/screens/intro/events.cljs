(ns polinka.screens.intro.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [re-frame.core :refer [reg-event-db trim-v]]))


(reg-event-db
  ::init
  [trim-v]
  (fn-traced [db [model]]
    (assoc db :model model)))
