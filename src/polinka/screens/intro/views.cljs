(ns polinka.screens.intro.views
  (:require
    [polinka.components.core :refer [meter]]
    [polinka.config :refer [intro-timeout]]
    [polinka.translate.views :refer [tr]]
    [reagent.core :as r]))


(defn- frames
  [n]
  (let [parts (/ 100 n)
        pcts (->> (range n)
                  (map #(* parts %)))
        times (->> pcts
                   (map #(* (/ % 100) intro-timeout)))]
    (map vector pcts (reverse times))))


(defn- countdown
  []
  (let [pct (r/atom 100)]
    (doseq [[p t] (frames 4)]
      (js/setTimeout #(reset! pct p) t))
    (fn []
      [meter @pct])))


(defn intro
  []
  [:<>
   [countdown]
   [:div.intro-screen
    [:h1 "Polínka"]
    [:p [tr "Art & Concept"]
     [:br]
     [:span.text-secondary
      "Helena Rosová"]]
    [:p [tr "Sound & code"]
     [:br]
     [:span.text-secondary
      "Ondřej Nejedlý"]]]])
