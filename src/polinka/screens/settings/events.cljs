(ns polinka.screens.settings.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [polinka.sound.events :as-alias sound]
    [polinka.translate.events :as-alias translate]
    [re-frame.core :refer [reg-event-db reg-event-fx trim-v]]))


(reg-event-db
  ::init
  [trim-v]
  (fn-traced [db [model]]
    (let [new-model {:sound-on? (:sounds/sound-on? db)
                     :language (:translate/language db)}]
      (assoc db :model (merge model new-model)))))


(reg-event-fx
  ::set
  [trim-v]
  (fn-traced [{db :db} [key val]]
    (cond->
      {:db (assoc-in db [:model key] val)}

      (= key :sound-on?)
      (assoc :dispatch [::sound/set val])

      (= key :language)
      (assoc :dispatch [::translate/set-lang val]))))
