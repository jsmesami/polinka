(ns polinka.screens.settings.views
  (:require
    [polinka.components.core :refer [link]]
    [polinka.config :as config]
    [polinka.screens.events :as-alias screens]
    [polinka.screens.settings.events :as-alias settings]
    [polinka.translate.views :refer [tr]]
    [re-frame.core :refer [dispatch]]))


(defn set-sound
  [sound-on?]
  [:<> [:p
        [tr "Sound:"]]
       [:p
        [link
         [tr "On"]
         :class [:link--button (when sound-on? :link--selected)]
         :on-click #(dispatch [::settings/set :sound-on? true])]
        [link
         [tr "Off"]
         :class [:link--button (when-not sound-on? :link--selected)]
         :on-click #(dispatch [::settings/set :sound-on? false])]]])


(defn set-lang
  [language]
  [:<> [:p
        [tr "Language:"]]
       [:p
        (for [lang config/supported-languages]
          ^{:key lang}
          [link
           lang
           :class [:link--button (when (= lang language) :link--selected)]
           :on-click #(dispatch [::settings/set :language lang])])]])


(defn settings
  [{:keys [sound-on? language]}]
  [:div.settings-screen
   [:h1 [tr "Settings"]]
   [set-sound sound-on?]
   [set-lang language]
   [:div.bottom-link
    [link
     [tr "Back"]
     :on-click #(dispatch [::screens/set :ballot])]]])
