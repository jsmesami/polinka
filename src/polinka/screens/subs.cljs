(ns polinka.screens.subs
  (:require
    [re-frame.core :refer [reg-sub]]))


(reg-sub
  ::model
  (fn [db]
    (:model db)))
