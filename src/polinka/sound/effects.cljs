(ns polinka.sound.effects
  (:require
    [polinka.sound.events :as-alias sound]
    [re-frame.core :refer [dispatch reg-fx]]
    [re-frame.db]))


(reg-fx
  ::play
  (fn [sound-key]
    (dispatch [::sound/play sound-key])))


(reg-fx
  ::play*
  (fn [sound]
    (.play sound)))
