(ns polinka.sound.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    ["howler" :refer [Howl]]
    [polinka.app.events :as-alias app]
    [polinka.config :as config]
    [polinka.sound.effects :as-alias sound]
    [polinka.storage.effects :as-alias storage]
    [polinka.utils.static :refer [static]]
    [reagent.format :refer [format]]
    [re-frame.core :refer [inject-cofx reg-event-fx trim-v]]))


(defn howl
  "Prepare sound matching sound-key"
  [sound-key]
  (->> (name sound-key)
       (format "/sounds/%s.mp3")
       static
       (hash-map :src)
       clj->js
       (Howl.)))


(reg-event-fx
  ::init
  [(inject-cofx ::storage/get :sound-on?)]
  (fn-traced [{:keys [db sound-on?]}]
    (let [sound-on? (if (nil? sound-on?)
                      config/sound-on?
                      sound-on?)
          sounds (->> [:intro :hit1 :hit2 :missed :won :lost]
                      (map (juxt identity howl))
                      (into {}))]
      {:db (assoc db :sounds/lookup sounds)
       :fx [[:dispatch [::set sound-on?]]
            [:dispatch [::app/ready! :sounds]]]})))


(reg-event-fx
  ::set
  [trim-v]
  (fn-traced [{db :db} [sound-on?]]
    {::storage/set [:sound-on? sound-on?]
     :db (assoc db :sounds/sound-on? sound-on?)}))


(reg-event-fx
  ::play
  [trim-v]
  (fn-traced [{db :db} [sound-key]]
    (let [sound (get-in db [:sounds/lookup sound-key])
          sound-on? (get db :sounds/sound-on? config/sound-on?)]
      (when (and sound sound-on?)
        {::sound/play* sound}))))
