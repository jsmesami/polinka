(ns polinka.storage.core
  "Browser localStorage utilities"
  (:require
    [polinka.storage.effects]))
