(ns polinka.storage.effects
  (:require
    [cljs.reader :as edn]
    [re-frame.core :refer [reg-cofx reg-fx]]))


(reg-cofx
  ::get
  (fn [cofx key]
    (let [value (edn/read-string (js/localStorage.getItem key))]
      (assoc cofx key value))))


(reg-fx
  ::set
  (fn [[key value]]
    (js/localStorage.setItem key (pr-str value))))
