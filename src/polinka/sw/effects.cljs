(ns polinka.sw.effects
  (:require
    [polinka.sw.events :as-alias sw]
    [polinka.analytics.events :as-alias analytics]
    [re-frame.core :refer [dispatch reg-fx]]))


(defn- on-load
  [worker sw-js]
  (-> worker
      (.register sw-js #js{:updateViaCache "none"})
      (.then (fn [registration]
               (.update registration)))
      (.catch #(dispatch [::analytics/event "failed_sw_registration"]))))


(defn register-worker [sw-js]
  (when-let [worker (.-serviceWorker js/navigator)]
    (.addEventListener js/window "load" (partial on-load worker sw-js))
    (.addEventListener worker "controllerchange" #(dispatch [::sw/new-version]))))


(reg-fx
  ::init
  (fn [sw-js]
    (when-not js/goog.DEBUG
      (register-worker sw-js))))
