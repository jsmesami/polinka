(ns polinka.sw.events
  (:require
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [re-frame.core :refer [reg-event-db]]))


(reg-event-db
  ::new-version
  (fn-traced [db]
    (assoc db :sw/new-version? true)))
