(ns polinka.sw.subs
  (:require
    [re-frame.core :refer [reg-sub]]))


(reg-sub
  ::new-version?
  (fn [db]
    (:sw/new-version? db)))
