(ns polinka.translate.core
  "I18n support"
  (:require
    [polinka.translate.effects]
    [polinka.translate.events]
    [polinka.translate.subs]))
