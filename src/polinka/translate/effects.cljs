(ns polinka.translate.effects
  (:require
    [polinka.config :as config]
    [re-frame.core :refer [reg-fx]]))


(reg-fx
  ::set-html-lang
  (fn [lang]
    (when (contains? config/supported-languages lang)
      (js/document.documentElement.setAttribute "lang" lang))))
