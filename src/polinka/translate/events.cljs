(ns polinka.translate.events
  (:require
    [clojure.edn :as edn]
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [goog.object]
    [polinka.app.events :as-alias app]
    [polinka.config :as config]
    [polinka.storage.effects :as-alias storage]
    [polinka.translate.effects :as-alias translate]
    [re-frame.core :refer [inject-cofx reg-event-fx trim-v]]))


(defn- get-messages
  []
  (->> (js/document.getElementById "app")
       .-dataset
       .-messages
       (edn/read-string)))


(reg-event-fx
  ::init
  [(inject-cofx ::storage/get :language)]
  (fn [{:keys [db language]}]
    (let [browser-lang (->> (goog.object/get js/navigator "language" "")
                            (re-find #"^\w{2}")
                            (config/supported-languages))
          lang (or language browser-lang config/language)]
      {:db (assoc db :translate/messages (get-messages))
       :fx [[:dispatch [::set-lang lang]]
            [:dispatch [::app/ready! :translate]]]})))


(reg-event-fx
  ::set-lang
  [trim-v]
  (fn-traced [{db :db} [language]]
    (when (contains? config/supported-languages language)
      {::translate/set-html-lang language
       ::storage/set [:language language]
       :db (assoc db :translate/language language)})))
