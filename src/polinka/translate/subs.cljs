(ns polinka.translate.subs
  (:require
    [re-frame.core :refer [reg-sub]]))


(reg-sub
  ::messages
  (fn [db]
    (:translate/messages db)))


(reg-sub
  ::language
  (fn [db]
    (:translate/language db)))
