(ns polinka.translate.views
  (:require
    [polinka.translate.subs :as-alias translate]
    [re-frame.core :refer [subscribe]]))


(defn tr
  [key]
  (let [messages @(subscribe [::translate/messages])
        lang @(subscribe [::translate/language])]
    (get-in messages [key lang] key)))
