(ns polinka.utils.bem
  (:require
    [clojure.string :refer [blank?]]))


(defn be
  [block element]
  (str (name block) "__" (name element)))

(defn bm
  [block modifiers]
  (let [block-name (name block)
        map-modifiers (comp (remove blank?)
                            (map name)
                            (map (partial str block-name "--")))]
    (->> (if (sequential? modifiers)
           modifiers
           (vector modifiers))
         (into [block-name] map-modifiers))))

(defn bem
  [block element modifiers]
  (bm (be block element) modifiers))
