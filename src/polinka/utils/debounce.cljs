(ns polinka.utils.debounce
  (:import
    [goog.async Debouncer]))


(defn debounce [listener interval]
  (let [debouncer (Debouncer. listener interval)]
    (fn [& args]
      (.apply (.-fire debouncer) debouncer (to-array args)))))
