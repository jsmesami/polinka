(ns polinka.utils.geometry)


(defn fit-rectangle-to-container
  [[rect_w rect_h] [cont_w cont_h]]
  (let [rect_ratio (/ rect_w rect_h)
        cont_ratio (/ cont_w cont_h)
        scale_ratio (if (>= rect_ratio cont_ratio)
                      (/ cont_w rect_w)
                      (/ cont_h rect_h))]
    [(* rect_w scale_ratio) (* rect_h scale_ratio)]))
