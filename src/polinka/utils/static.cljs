(ns polinka.utils.static
  "Dealing with static files paths"
  (:require
    [clojure.edn :as edn]
    [reagent.format :refer [format]]))


(defonce
  statics-lookup
  (->> (js/document.getElementById "app")
       .-dataset
       .-statics
       (edn/read-string)))


(defn with-ver
  [uri ver]
  (if ver
    (format "%s?v=%s" uri ver)
    uri))


(defn static
  [uri]
  (let [ver (get statics-lookup uri)]
    (with-ver uri ver)))
