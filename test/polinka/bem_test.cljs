(ns polinka.bem-test
  (:require
    [cljs.test :refer [are deftest testing]]
    [polinka.utils.bem :refer [be bm bem]]))


(deftest bem-test
  (testing "BEM utils"
    (testing "polinka.utils.bem/bem"
      (are [provided expected]
        (= (apply bem provided) expected)

        [:block :element [:modifier1 :modifier2]]
        ["block__element" "block__element--modifier1" "block__element--modifier2"]

        ["block" "element" ["modifier1" "modifier2"]]
        ["block__element" "block__element--modifier1" "block__element--modifier2"]

        [:block :element :modifier]
        ["block__element" "block__element--modifier"]

        ["block" "element" "modifier"]
        ["block__element" "block__element--modifier"]))

    (testing "polinka.utils.bem/be"
      (are [provided expected]
        (= (apply be provided) expected)

        [:block :element]
        "block__element"

        ["block" "element"]
        "block__element"))

    (testing "polinka.utils.bem/bm"
      (are [provided expected]
        (= (apply bm provided) expected)

        [:block :modifier]
        ["block" "block--modifier"]

        ["block" "modifier"]
        ["block" "block--modifier"]))))
