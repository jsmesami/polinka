(ns polinka.game1-pairs-test
  (:require
    [clojure.test :refer [deftest is testing]]
    [polinka.screens.game1-pairs.db :refer [init-model]]
    [polinka.screens.game1-pairs.events :refer [advance-game max-taken-count] :as game1-pairs]))


(deftest game1-pairs-test
  (testing ::game1-pairs/advance-game
    (let [model (init-model)
          db {:model model}]
      (testing "winning gameplay"
        (let [pairs (-> (group-by :image-id (:fields model))
                        (dissoc nil)
                        vals)
              winning-moves (map :id (apply concat pairs))
              almost-won-db (reduce #(:db (advance-game %1 %2))
                                    db
                                    (butlast winning-moves))
              won-fx (advance-game almost-won-db (last winning-moves))
              new-model (get-in won-fx [:db :model])
              taken-fields (filter :taken? (:fields new-model))]

          (is (= max-taken-count
                 (:taken-count new-model)
                 (count taken-fields)
                 (count winning-moves)))
          (is (= (:progress new-model)
                 100))
          (is (= (map :rotation (:fields new-model))
                 (map :rotation (:fields model))))
          (is (= (:polinka.game.effects/success won-fx)
                 ["Pairs" (:score new-model)]))))

      (testing "wrong move"
        (let [f1 (first (->> model :fields))
              f2 (first (->> model :fields (remove #(= (:image-id %) (:image-id f1)))))
              info-f? (comp nil? :image-id)

              step1-fx (advance-game db (:id f1))
              step1-model (get-in step1-fx [:db :model])
              step1-fields (:fields step1-model)

              step2-fx (advance-game (:db step1-fx) (:id f2))
              step2-model (get-in step2-fx [:db :model])
              step2-fields (:fields step2-model)]

          (is (= (select-keys step1-model [:taken-count :progress :score])
                 {:taken-count 0, :progress 0, :score 0}))
          (is (:selected? (first step1-fields)))
          (is (every? false? (map (comp boolean :selected?) (rest step1-fields))))
          (is (every? false? (map (comp boolean :taken?) step1-fields)))
          (is (every? true?
                      (map = (map :rotation (remove info-f? (:fields model)))
                             (map :rotation (remove info-f? step1-fields)))))

          (is (= (select-keys step2-model [:taken-count :progress :score])
                 {:taken-count 0, :progress 0, :score 0}))
          (is (:selected? (first step2-fields)))
          (is (every? false? (map (comp boolean :selected?) (rest step2-fields))))
          (is (every? false? (map (comp boolean :taken?) step2-fields)))
          (is (every? false?
                      (map = (map :rotation (remove info-f? (:fields model)))
                             (map :rotation (remove info-f? step2-fields)))))))
      ,)))
