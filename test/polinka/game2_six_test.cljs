(ns polinka.game2-six-test
  (:require
    [clojure.test :refer [deftest is testing]]
    [polinka.screens.game2-six.db :refer [init-model]]
    [polinka.screens.game2-six.events :refer [advance-game max-taken-count] :as game2-six]))


(deftest game2-six-test
  (testing ::game2-six/advance-game
    (let [model (init-model)
          db {:model model}
          ref-iid? #(= (:reference-image-id model) (:image-id %))]
      (testing "winning gameplay"
        (let [winning-moves (->> (:fields model)
                                 (filter ref-iid?)
                                 (map :id))
              almost-won-db (reduce #(:db (advance-game %1 %2))
                                    db
                                    (butlast winning-moves))
              won-fx (advance-game almost-won-db (last winning-moves))
              new-model (get-in won-fx [:db :model])
              taken-fields (filter :taken? (:fields new-model))]

          (is (= max-taken-count
                 (:taken-count new-model)
                 (count taken-fields)
                 (count winning-moves)))
          (is (= winning-moves
                 (map :id taken-fields)))
          (is (= (:progress new-model)
                 100))
          (is (= (map :rotation (:fields new-model))
                 (map :rotation (:fields model))))
          (is (= (:polinka.game.effects/success won-fx)
                 ["Six" (:score new-model)]))))

      (testing "wrong move"
        (let [wrong-f (->> (:fields model)
                           (remove ref-iid?)
                           first)
              info-f? (comp nil? :image-id)
              fx (advance-game db (:id wrong-f))
              new-model (get-in fx [:db :model])
              new-fields (:fields new-model)]

          (is (= (select-keys new-model [:taken-count :progress :score])
                 {:taken-count 0, :progress 0, :score 0}))
          (is (every? false? (map (comp boolean :taken?) new-fields)))
          (is (every? false?
                      (map = (map :rotation (remove info-f? (:fields model)))
                             (map :rotation (remove info-f? new-fields)))))))
      ,)))
